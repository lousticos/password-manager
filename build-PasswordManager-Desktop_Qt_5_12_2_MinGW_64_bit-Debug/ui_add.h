/********************************************************************************
** Form generated from reading UI file 'add.ui'
**
** Created by: Qt User Interface Compiler version 5.12.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ADD_H
#define UI_ADD_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QFrame>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QTextEdit>

QT_BEGIN_NAMESPACE

class Ui_Add
{
public:
    QDialogButtonBox *buttonBox;
    QLineEdit *add_guser;
    QLabel *label;
    QLabel *label_2;
    QLineEdit *add_gpass;
    QFrame *frame;
    QLabel *label_3;
    QLabel *label_4;
    QLabel *label_5;
    QLabel *label_6;
    QLineEdit *add_user;
    QLineEdit *add_name;
    QLineEdit *add_pass;
    QLineEdit *add_type;
    QLabel *label_7;
    QTextEdit *add_desc;

    void setupUi(QDialog *Add)
    {
        if (Add->objectName().isEmpty())
            Add->setObjectName(QString::fromUtf8("Add"));
        Add->resize(596, 456);
        Add->setMinimumSize(QSize(596, 456));
        Add->setMaximumSize(QSize(596, 456));
        buttonBox = new QDialogButtonBox(Add);
        buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
        buttonBox->setGeometry(QRect(420, 401, 161, 51));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);
        add_guser = new QLineEdit(Add);
        add_guser->setObjectName(QString::fromUtf8("add_guser"));
        add_guser->setGeometry(QRect(70, 400, 331, 20));
        label = new QLabel(Add);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(10, 400, 61, 20));
        label_2 = new QLabel(Add);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setGeometry(QRect(10, 430, 61, 20));
        add_gpass = new QLineEdit(Add);
        add_gpass->setObjectName(QString::fromUtf8("add_gpass"));
        add_gpass->setGeometry(QRect(70, 430, 331, 20));
        add_gpass->setEchoMode(QLineEdit::Password);
        frame = new QFrame(Add);
        frame->setObjectName(QString::fromUtf8("frame"));
        frame->setGeometry(QRect(10, 10, 571, 381));
        frame->setFrameShape(QFrame::StyledPanel);
        frame->setFrameShadow(QFrame::Plain);
        label_3 = new QLabel(frame);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setGeometry(QRect(10, 10, 51, 16));
        label_4 = new QLabel(frame);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setGeometry(QRect(10, 40, 61, 16));
        label_5 = new QLabel(frame);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        label_5->setGeometry(QRect(290, 10, 47, 16));
        label_6 = new QLabel(frame);
        label_6->setObjectName(QString::fromUtf8("label_6"));
        label_6->setGeometry(QRect(290, 40, 61, 16));
        add_user = new QLineEdit(frame);
        add_user->setObjectName(QString::fromUtf8("add_user"));
        add_user->setGeometry(QRect(70, 40, 211, 20));
        add_name = new QLineEdit(frame);
        add_name->setObjectName(QString::fromUtf8("add_name"));
        add_name->setGeometry(QRect(70, 10, 211, 20));
        add_pass = new QLineEdit(frame);
        add_pass->setObjectName(QString::fromUtf8("add_pass"));
        add_pass->setGeometry(QRect(350, 40, 211, 20));
        add_pass->setEchoMode(QLineEdit::Password);
        add_type = new QLineEdit(frame);
        add_type->setObjectName(QString::fromUtf8("add_type"));
        add_type->setGeometry(QRect(350, 10, 211, 20));
        label_7 = new QLabel(frame);
        label_7->setObjectName(QString::fromUtf8("label_7"));
        label_7->setGeometry(QRect(10, 70, 71, 16));
        add_desc = new QTextEdit(frame);
        add_desc->setObjectName(QString::fromUtf8("add_desc"));
        add_desc->setGeometry(QRect(10, 90, 551, 281));

        retranslateUi(Add);
        QObject::connect(buttonBox, SIGNAL(accepted()), Add, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), Add, SLOT(reject()));

        QMetaObject::connectSlotsByName(Add);
    } // setupUi

    void retranslateUi(QDialog *Add)
    {
        Add->setWindowTitle(QApplication::translate("Add", "Dialog", nullptr));
        label->setText(QApplication::translate("Add", "Username :", nullptr));
        label_2->setText(QApplication::translate("Add", "Password :", nullptr));
        label_3->setText(QApplication::translate("Add", "Name :", nullptr));
        label_4->setText(QApplication::translate("Add", "Username :", nullptr));
        label_5->setText(QApplication::translate("Add", "Type :", nullptr));
        label_6->setText(QApplication::translate("Add", "Password :", nullptr));
        label_7->setText(QApplication::translate("Add", "Description :", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Add: public Ui_Add {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ADD_H
