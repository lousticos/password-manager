/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.12.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QStackedWidget>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTableView>
#include <QtWidgets/QTextBrowser>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionadd_password;
    QAction *actiondelete_password;
    QAction *actionouvrir;
    QAction *actionenregistrer;
    QAction *actionnew;
    QWidget *centralWidget;
    QHBoxLayout *horizontalLayout;
    QTableView *tableView;
    QStackedWidget *stackedWidget;
    QWidget *service;
    QGridLayout *gridLayout;
    QLabel *nameTitle_2;
    QLabel *name_2;
    QLabel *typeTitle_2;
    QLabel *type_2;
    QLabel *descriptionTitle_2;
    QTextBrowser *description;
    QWidget *page_3;
    QVBoxLayout *verticalLayout_2;
    QSpacerItem *verticalSpacer_4;
    QHBoxLayout *horizontalLayout_2;
    QLabel *userTitle;
    QLabel *user;
    QSpacerItem *verticalSpacer_3;
    QHBoxLayout *horizontalLayout_5;
    QLineEdit *lineEdit;
    QPushButton *pushButton;
    QWidget *page;
    QVBoxLayout *verticalLayout;
    QSpacerItem *verticalSpacer_2;
    QHBoxLayout *horizontalLayout_3;
    QLabel *pswTitle;
    QLabel *psw;
    QPushButton *pushButton_2;
    QSpacerItem *verticalSpacer;
    QHBoxLayout *horizontalLayout_4;
    QLineEdit *lineEdit_2;
    QPushButton *pushButton_3;
    QMenuBar *menuBar;
    QMenu *menuse_connecter;
    QMenu *menufichier;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(1212, 668);
        MainWindow->setMinimumSize(QSize(600, 400));
        actionadd_password = new QAction(MainWindow);
        actionadd_password->setObjectName(QString::fromUtf8("actionadd_password"));
        actiondelete_password = new QAction(MainWindow);
        actiondelete_password->setObjectName(QString::fromUtf8("actiondelete_password"));
        actionouvrir = new QAction(MainWindow);
        actionouvrir->setObjectName(QString::fromUtf8("actionouvrir"));
        actionenregistrer = new QAction(MainWindow);
        actionenregistrer->setObjectName(QString::fromUtf8("actionenregistrer"));
        actionnew = new QAction(MainWindow);
        actionnew->setObjectName(QString::fromUtf8("actionnew"));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        horizontalLayout = new QHBoxLayout(centralWidget);
        horizontalLayout->setSpacing(6);
        horizontalLayout->setContentsMargins(11, 11, 11, 11);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        tableView = new QTableView(centralWidget);
        tableView->setObjectName(QString::fromUtf8("tableView"));
        tableView->setMaximumSize(QSize(302, 16777215));

        horizontalLayout->addWidget(tableView);

        stackedWidget = new QStackedWidget(centralWidget);
        stackedWidget->setObjectName(QString::fromUtf8("stackedWidget"));
        service = new QWidget();
        service->setObjectName(QString::fromUtf8("service"));
        gridLayout = new QGridLayout(service);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        nameTitle_2 = new QLabel(service);
        nameTitle_2->setObjectName(QString::fromUtf8("nameTitle_2"));
        nameTitle_2->setMaximumSize(QSize(60, 16777215));

        gridLayout->addWidget(nameTitle_2, 0, 0, 1, 1);

        name_2 = new QLabel(service);
        name_2->setObjectName(QString::fromUtf8("name_2"));

        gridLayout->addWidget(name_2, 0, 1, 1, 1);

        typeTitle_2 = new QLabel(service);
        typeTitle_2->setObjectName(QString::fromUtf8("typeTitle_2"));
        typeTitle_2->setMaximumSize(QSize(60, 16777215));

        gridLayout->addWidget(typeTitle_2, 1, 0, 1, 1);

        type_2 = new QLabel(service);
        type_2->setObjectName(QString::fromUtf8("type_2"));

        gridLayout->addWidget(type_2, 1, 1, 1, 1);

        descriptionTitle_2 = new QLabel(service);
        descriptionTitle_2->setObjectName(QString::fromUtf8("descriptionTitle_2"));

        gridLayout->addWidget(descriptionTitle_2, 2, 0, 1, 1);

        description = new QTextBrowser(service);
        description->setObjectName(QString::fromUtf8("description"));

        gridLayout->addWidget(description, 3, 0, 1, 2);

        stackedWidget->addWidget(service);
        page_3 = new QWidget();
        page_3->setObjectName(QString::fromUtf8("page_3"));
        verticalLayout_2 = new QVBoxLayout(page_3);
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        verticalSpacer_4 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_2->addItem(verticalSpacer_4);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        userTitle = new QLabel(page_3);
        userTitle->setObjectName(QString::fromUtf8("userTitle"));
        userTitle->setMinimumSize(QSize(0, 0));
        userTitle->setMaximumSize(QSize(16777215, 16777215));
        userTitle->setAlignment(Qt::AlignCenter);

        horizontalLayout_2->addWidget(userTitle);

        user = new QLabel(page_3);
        user->setObjectName(QString::fromUtf8("user"));
        user->setMinimumSize(QSize(0, 0));
        user->setMaximumSize(QSize(16777215, 16777215));
        user->setScaledContents(false);
        user->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
        user->setWordWrap(false);

        horizontalLayout_2->addWidget(user);


        verticalLayout_2->addLayout(horizontalLayout_2);

        verticalSpacer_3 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_2->addItem(verticalSpacer_3);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setSpacing(6);
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        lineEdit = new QLineEdit(page_3);
        lineEdit->setObjectName(QString::fromUtf8("lineEdit"));

        horizontalLayout_5->addWidget(lineEdit);

        pushButton = new QPushButton(page_3);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));

        horizontalLayout_5->addWidget(pushButton);


        verticalLayout_2->addLayout(horizontalLayout_5);

        stackedWidget->addWidget(page_3);
        page = new QWidget();
        page->setObjectName(QString::fromUtf8("page"));
        verticalLayout = new QVBoxLayout(page);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer_2);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        pswTitle = new QLabel(page);
        pswTitle->setObjectName(QString::fromUtf8("pswTitle"));
        pswTitle->setMinimumSize(QSize(0, 0));
        pswTitle->setAlignment(Qt::AlignCenter);

        horizontalLayout_3->addWidget(pswTitle);

        psw = new QLabel(page);
        psw->setObjectName(QString::fromUtf8("psw"));
        psw->setMinimumSize(QSize(0, 0));
        psw->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        horizontalLayout_3->addWidget(psw);

        pushButton_2 = new QPushButton(page);
        pushButton_2->setObjectName(QString::fromUtf8("pushButton_2"));

        horizontalLayout_3->addWidget(pushButton_2);


        verticalLayout->addLayout(horizontalLayout_3);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setSpacing(6);
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        lineEdit_2 = new QLineEdit(page);
        lineEdit_2->setObjectName(QString::fromUtf8("lineEdit_2"));
        lineEdit_2->setMinimumSize(QSize(400, 0));

        horizontalLayout_4->addWidget(lineEdit_2);

        pushButton_3 = new QPushButton(page);
        pushButton_3->setObjectName(QString::fromUtf8("pushButton_3"));

        horizontalLayout_4->addWidget(pushButton_3);


        verticalLayout->addLayout(horizontalLayout_4);

        stackedWidget->addWidget(page);

        horizontalLayout->addWidget(stackedWidget);

        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 1212, 21));
        menuse_connecter = new QMenu(menuBar);
        menuse_connecter->setObjectName(QString::fromUtf8("menuse_connecter"));
        menufichier = new QMenu(menuBar);
        menufichier->setObjectName(QString::fromUtf8("menufichier"));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QString::fromUtf8("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        MainWindow->setStatusBar(statusBar);

        menuBar->addAction(menufichier->menuAction());
        menuBar->addAction(menuse_connecter->menuAction());
        menuse_connecter->addSeparator();
        menuse_connecter->addSeparator();
        menuse_connecter->addAction(actionadd_password);
        menuse_connecter->addAction(actiondelete_password);
        menufichier->addAction(actionnew);
        menufichier->addAction(actionouvrir);
        menufichier->addAction(actionenregistrer);

        retranslateUi(MainWindow);

        stackedWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", nullptr));
        actionadd_password->setText(QApplication::translate("MainWindow", "add password", nullptr));
        actiondelete_password->setText(QApplication::translate("MainWindow", "delete password", nullptr));
        actionouvrir->setText(QApplication::translate("MainWindow", "open", nullptr));
        actionenregistrer->setText(QApplication::translate("MainWindow", "save", nullptr));
        actionnew->setText(QApplication::translate("MainWindow", "new", nullptr));
        nameTitle_2->setText(QApplication::translate("MainWindow", "Nom :", nullptr));
        name_2->setText(QString());
        typeTitle_2->setText(QApplication::translate("MainWindow", "Type :", nullptr));
        type_2->setText(QString());
        descriptionTitle_2->setText(QApplication::translate("MainWindow", "Description :", nullptr));
        userTitle->setText(QApplication::translate("MainWindow", "Username :", nullptr));
        user->setText(QString());
        pushButton->setText(QApplication::translate("MainWindow", "Change", nullptr));
        pswTitle->setText(QApplication::translate("MainWindow", "Password :", nullptr));
        psw->setText(QApplication::translate("MainWindow", "********", nullptr));
        pushButton_2->setText(QApplication::translate("MainWindow", "View", nullptr));
        pushButton_3->setText(QApplication::translate("MainWindow", "Change", nullptr));
        menuse_connecter->setTitle(QApplication::translate("MainWindow", "edit", nullptr));
        menufichier->setTitle(QApplication::translate("MainWindow", "file", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
