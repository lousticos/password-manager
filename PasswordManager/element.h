#ifndef ELEMENT_H
#define ELEMENT_H

#include "service.h"
#include "password.h"
#include "username.h"

class Element
{
public:
    Element(Service serv,std::string user, std::string pass, std::string gUsername, std::string gPassword);
    Element();
    Service* getService();
    Username* getUsername();
    Password* getPassword();
    void setUsername(std::string username,std::string gUsername,std::string gPassword);
    void setPassword(std::string username,std::string gUsername,std::string gPassword);
private:
    Service* serv;
    Username* user;
    Password* pass;
};

#endif // ELEMENT_H
