#ifndef VERIFY_H
#define VERIFY_H

#include <QDialog>

namespace Ui {
class Verify;
}

class Verify : public QDialog
{
    Q_OBJECT

public:
    explicit Verify(QWidget *parent = nullptr);
    ~Verify();
    QString getUsername();
    QString getPassword();

private slots:
    void on_buttonBox_accepted();

    void on_buttonBox_rejected();

private:
    Ui::Verify *ui;
    QString username;
    QString password;
};

#endif // VERIFY_H
