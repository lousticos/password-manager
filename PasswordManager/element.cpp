#include "element.h"
#include <iostream>

Element::Element(Service serv,std::string user, std::string pass, std::string gUsername, std::string gPassword)
{
    this->serv = new Service(serv.getName(),serv.getType(),serv.getDesc());
    this->user = new Username(user,gUsername,gPassword);
    this->pass = new Password(pass,gUsername,gPassword);
}

Element::Element() :
    serv(),
    user(),
    pass()
{
}


Service* Element::getService(){
    return serv;
}

Username* Element::getUsername(){
    return user;
}

Password* Element::getPassword(){
    return pass;
}

void Element::setUsername(std::string username,std::string gUsername,std::string gPassword){
    this->user->setUsername(username,gUsername,gPassword);
}

void Element::setPassword(std::string username,std::string gUsername,std::string gPassword){
    this->pass->setPassword(username,gUsername,gPassword);
}
