#ifndef SERVICE_H
#define SERVICE_H
#include <string>

class Service
{
public:
    Service(std::string name,std::string type,std::string description);
    Service();
    std::string getName();
    std::string getType();
    std::string getDesc();
    void setName(std::string name);
    void setType(std::string type);
    void setDesc(std::string type);
private:
    std::string name;
    std::string type;
    std::string description;
};

#endif // SERVICE_H
