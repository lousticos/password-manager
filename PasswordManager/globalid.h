#ifndef GLOBALID_H
#define GLOBALID_H

#include <string>

class GlobalID
{
public:
    GlobalID(std::string username, std::string password);
    bool verify(std::string username,std::string password);
private:
    std::string username;
    std::string password;
};

#endif // GLOBALID_H
