#include "elements.h"

Elements::Elements(QObject *parent)
    : QAbstractTableModel(parent)
{

}

void Elements::addElement(Element element){
    elementVector.push_back(element);
}

void Elements::deleteElement(Element* element){
    for(std::vector<Element>::iterator it = elementVector.begin() ; it < elementVector.end() ; it++){
        if(it->getService() == element->getService()){
            elementVector.erase(it);
        }

    }
}

bool Elements::insertRows(int position, int rows, const QModelIndex &parent){
    beginInsertRows(QModelIndex(),position,position+rows-1);
    endInsertRows();
    (void)parent;
    return true;

}

bool Elements::removeRows(int position, int rows, const QModelIndex &parent){
    beginRemoveRows(QModelIndex(),position,position+rows-1);
    endRemoveRows();
    (void)parent;
    return true;
}

int Elements::getSize(){
    return static_cast<int>(elementVector.size());
}

bool Elements::isEmpty(){
    return elementVector.empty();
}

void Elements::clear(){
    elementVector.clear();
}

int Elements::importFile(QFile &filePath,QString gUsername,QString gPassword){


    if (!filePath.open(QIODevice::ReadOnly)) {
        exit(1);
    }

    QByteArray saveData = filePath.readAll();
    QJsonDocument loadDoc(QJsonDocument::fromJson(saveData));
    QJsonArray dataArray = loadDoc.array();
    for(int i = 0; i<dataArray.size() ; i++){
        QJsonArray data = dataArray.at(i).toArray();
        Service serv(data.at(0).toString().toUtf8().constData(),data.at(1).toString().toUtf8().constData(),data.at(2).toString().toUtf8().constData());
        std::string code = data.at(4).toString().toUtf8().constData();
        std::string resu;
        std::string gPasswordStr = gPassword.toUtf8().constData();
        int nb = 0;
        int n = gPassword.size();
        unsigned j = 0;
        for(char& c : code){
            nb = static_cast<int>(c)-static_cast<int>(gPasswordStr[j]);
            if (nb < 0) nb = nb + 127;
            resu+=static_cast<char>(nb);
            j++;

            if (static_cast<int>(j)==n) j=0;
        }
        Element elt(serv,data.at(3).toString().toUtf8().constData(),resu,gUsername.toUtf8().constData(),gPasswordStr);
        this->elementVector.push_back(elt);
    }
    return dataArray.size();
}

void Elements::exportFile(QFile  &filePath){
    QJsonArray dataArray;
    for( unsigned i =0; i < elementVector.size() ; i++){
        Element value = elementVector[i];
        QJsonArray data;
        QJsonValue name(QString::fromStdString(value.getService()->getName()));
        data.append(name);
        QJsonValue type(QString::fromStdString(value.getService()->getType()));
        data.append(type);
        QJsonValue desc(QString::fromStdString(value.getService()->getDesc()));
        data.append(desc);
        QJsonValue username(QString::fromStdString(value.getUsername()->getUsername()));
        data.append(username);
        QJsonValue password(QString::fromStdString(value.getPassword()->getCode()));
        data.append(password);
        dataArray.append(data);
    }

    if (!filePath.open(QIODevice::WriteOnly)) {
       exit(1);
    }

    QJsonDocument saveDoc(dataArray);
    filePath.write(saveDoc.toJson());
}

int Elements::rowCount(const QModelIndex &parent) const{
    (void)parent;
    return static_cast<int>(elementVector.size());
}

int Elements::columnCount(const QModelIndex &parent) const{
    (void)parent;
    return 3;
}

QVariant Elements::data(const QModelIndex & index, int role) const{
    if (role == Qt::DisplayRole){
        Element name = elementVector.at(static_cast<unsigned>(index.row()));
        if(index.column()==2){
            return QString("********");
        }else if(index.column()==1){
            if(name.getUsername()->isVisible()){
                return QString(QString::fromStdString(name.getUsername()->getUsername("")));
            }else{
                return QString("********");
            }
        }else{
            return QString(QString::fromStdString(name.getService()->getName()));
        }


    }
    return QVariant();
}

QVariant Elements::headerData(int section, Qt::Orientation orientation, int role) const{
    if(orientation == Qt::Horizontal && role == Qt::DisplayRole){
        if (section == 0){
            return QString("service name");
        }else if(section == 1){
            return QString("username");
        }else if(section == 2){
            return QString("password");
        }
    }
    return QVariant();
}

Element* Elements::getElement(const QModelIndex &index){

    return &(*(elementVector.begin()+index.row()));
}
