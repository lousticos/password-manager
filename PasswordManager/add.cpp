#include "add.h"
#include "ui_add.h"

Add::Add(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Add),
    username(""),
    password(""),
    gusername(""),
    gpassword(""),
    name(""),
    type(""),
    description("")
{
    ui->setupUi(this);
}

Add::~Add()
{
    delete ui;
}

QString Add::getUsername(){
    return username;
}

QString Add::getPassword(){
    return password;
}

QString Add::getGUsername(){
    return gusername;
}

QString Add::getGPassword(){
    return gpassword;
}

QString Add::getName(){
    return name;
}

QString Add::getType(){
    return type;
}

QString Add::getDescription(){
    return description;
}

void Add::on_buttonBox_accepted()
{
    username = ui->add_user->text();
    password = ui->add_pass->text();
    gusername = ui->add_guser->text();
    gpassword = ui->add_gpass->text();
    name = ui->add_name->text();
    type = ui->add_type->text();
    description = ui->add_desc->toPlainText();
}
