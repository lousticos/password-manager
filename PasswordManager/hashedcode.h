#ifndef HASHEDCODE_H
#define HASHEDCODE_H

#include <string>

class HashedCode
{
public:
    HashedCode(std::string code,std::string key);
    std::string getCode(std::string key);
    std::string getCode();
    void setCode(std::string code, std::string key);
private:
    std::string code;
};

#endif // HASHEDCODE_H
