#include "verify.h"
#include "ui_verify.h"

Verify::Verify(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Verify),
    username(""),
    password("")
{
    ui->setupUi(this);
}

Verify::~Verify()
{
    delete ui;
}

QString Verify::getUsername(){
    return username;
}

QString Verify::getPassword(){
    return password;
}

void Verify::on_buttonBox_accepted()
{
    username = ui->lineEdit->text();
    password = ui->lineEdit_2->text();
}

void Verify::on_buttonBox_rejected()
{
    username = "";
    password = "";
}
