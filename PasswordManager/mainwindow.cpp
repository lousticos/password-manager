#include "mainwindow.h"
#include "ui_mainwindow.h"


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    currElem(),
    saved(true)
{
    data = new Elements();
    ui->setupUi(this);
    QFile loadFile(QStringLiteral("save.json"));

    /*data->importFile(loadFile,"lousticos","1234");*/

    ui->tableView->setModel(data);
    data->setHeaderData(0,Qt::Horizontal,QObject::tr("Annual Pay"));
    data->setHeaderData(1,Qt::Horizontal,"username");
    data->setHeaderData(2,Qt::Horizontal,"password");
    connect(ui->tableView,SIGNAL(clicked(QModelIndex)),this,SLOT(stackedWidgetChange(QModelIndex)));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::closeEvent(QCloseEvent *event){
    if(!saved){
        QMessageBox::StandardButton resBtn = QMessageBox::question( this, "Save data ?",tr("Would you like to save your data ?\n"),QMessageBox::Cancel | QMessageBox::No | QMessageBox::Yes,QMessageBox::Yes);
        if (resBtn == QMessageBox::Yes) {
            QFileDialog box1;
            QString path = box1.getSaveFileName();
            if(path != ""){
                path = path + ".json";
                QFile loadfile(path);
                data->exportFile(loadfile);
                saved = true;
            }
            event->accept();
        } else if(resBtn == QMessageBox::No){
            event->accept();
        } else{
            event->ignore();
        }
    }
    event->accept();
}

void MainWindow::stackedWidgetChange(QModelIndex ind){
    currElem = data->getElement(ind);
    ui->name_2->setText(QString::fromStdString(currElem->getService()->getName()));
    ui->type_2->setText(QString::fromStdString(currElem->getService()->getType()));
    ui->description->setPlainText(QString::fromStdString(currElem->getService()->getDesc()));
    if(currElem->getUsername()->isVisible()){
        ui->user->setText(QString::fromStdString(currElem->getUsername()->getUsername("")));
    }else{
        ui->user->setText("********");
    }
    ui->psw->setText("********");
    ui->stackedWidget->setCurrentIndex(ind.column());


}

void MainWindow::on_pushButton_2_clicked()
{
    Verify box;
    if(box.exec()==1){
        std::string usr = box.getUsername().toStdString();
        std::string psw = box.getPassword().toStdString();
        if(usr != "" && psw != ""){
            ui->psw->setText(QString::fromStdString(currElem->getPassword()->getCode(usr,psw)));
        }
    }

}

void MainWindow::on_pushButton_3_clicked()
{

    Verify box;
    if(box.exec()==1){
        std::string usr = box.getUsername().toStdString();
        std::string psw = box.getPassword().toStdString();
        std::string newPsw = ui->lineEdit_2->text().toStdString();
        if(usr != "" && psw != "" && newPsw != ""){
            currElem->setPassword(newPsw,usr,psw);
            saved = false;
        }
        ui->psw->setText(QString::fromStdString(currElem->getPassword()->getCode(usr,psw)));
    }

}

void MainWindow::on_pushButton_clicked()
{
    Verify box;
    if(box.exec()==1){

        std::string usr = box.getUsername().toStdString();
        std::string psw = box.getPassword().toStdString();
        std::string newUsr = ui->lineEdit->text().toStdString();
        if(usr != "" && psw != "" && newUsr != ""){
            currElem->setUsername(newUsr,usr,psw);
            saved = false;
        }
        if(currElem->getUsername()->isVisible()){
            ui->user->setText(QString::fromStdString(currElem->getUsername()->getUsername("")));
        }else{
            ui->user->setText("********");
        }
    }
}

void MainWindow::on_actionadd_password_triggered()
{
    Add box;
    if(box.exec()==1){
        std::string usr = box.getUsername().toStdString();
        std::string psw = box.getPassword().toStdString();
        std::string gusr = box.getGUsername().toStdString();
        std::string gpsw = box.getGPassword().toStdString();
        std::string name = box.getName().toStdString();
        std::string type = box.getType().toStdString();
        std::string desc = box.getDescription().toStdString();
        if(usr != "" && psw != "" && gusr != "" && gpsw != "" && name != "" && type != "" && desc != ""){

            Service serv(name,type,desc);
            Element elt(serv,usr,psw,gusr,gpsw);
            data->addElement(elt);
            data->insertRows(data->getSize()-1,1,QModelIndex());
            saved = false;
        }
    }
}

void MainWindow::on_actiondelete_password_triggered()
{
    if(currElem!=nullptr){
        Verify box;
        if(box.exec()){
            std::string usr = box.getUsername().toStdString();
            std::string psw = box.getPassword().toStdString();
            if(currElem->getPassword()->getCode(usr,psw) != "wrong"){
                data->removeRows(0,1,QModelIndex());
                data->deleteElement(currElem);
                currElem = nullptr;
                ui->name_2->setText(QString(""));
                ui->type_2->setText(QString(""));
                ui->description->setPlainText(QString(""));
                ui->user->setText(QString(""));
                ui->psw->setText(QString(""));
                saved = false;
            }

        }
    }

}

void MainWindow::on_actionouvrir_triggered()
{
    QFileDialog box1;
    Verify box2;
    if(!saved){
        QFileDialog box1;
        QString path = box1.getSaveFileName();
        if(path != ""){
            path = path + ".json";
            QFile loadfile(path);
            data->exportFile(loadfile);
            saved = true;
        }
    }
    QString path = box1.getOpenFileName();
    if(path != ""){
        if(box2.exec()){
            std::string usr = box2.getUsername().toStdString();
            std::string psw = box2.getPassword().toStdString();
            if(usr != "" && psw != ""){

                    QFile loadfile(path);
                    if(!data->isEmpty()){
                        data->removeRows(0,data->getSize(),QModelIndex());
                        data->clear();
                    }
                    int nb = data->importFile(loadfile,box2.getUsername(),box2.getPassword());
                    data->insertRows(data->getSize()-1,nb,QModelIndex());
                    saved = true;
                }
            }
    }
}

void MainWindow::on_actionenregistrer_triggered()
{
    QFileDialog box1;
    QString path = box1.getSaveFileName();
    if(path != ""){
        path = path + ".json";
        QFile loadfile(path);
        data->exportFile(loadfile);
        saved = true;
    }
}

void MainWindow::on_actionnew_triggered()
{
    if(!saved){
        QFileDialog box1;
        QString path = box1.getSaveFileName();
        if(path != ""){
            path = path + ".json";
            QFile loadfile(path);
            data->exportFile(loadfile);
            saved = true;
        }
    }
    if(!data->isEmpty()){
        data->removeRows(0,data->getSize(),QModelIndex());
        data->clear();
    }
    saved = true;
}
