#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "elements.h"
#include "verify.h"
#include <iostream>
#include "add.h"
#include <QFileDialog>
#include <QCloseEvent>
#include <QMessageBox>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    void closeEvent (QCloseEvent *event);

private slots:
    void stackedWidgetChange(QModelIndex);

    void on_pushButton_2_clicked();

    void on_pushButton_3_clicked();

    void on_pushButton_clicked();

    void on_actionadd_password_triggered();

    void on_actiondelete_password_triggered();

    void on_actionouvrir_triggered();

    void on_actionenregistrer_triggered();

    void on_actionnew_triggered();

private:
    Ui::MainWindow *ui;
    Elements* data;
    Element* currElem;
    bool saved;
};

#endif // MAINWINDOW_H
