#ifndef USERNAME_H
#define USERNAME_H

#include "globalid.h"
#include "hashedcode.h"

class Username
{
public:
    Username(std::string username, std::string gUsername, std::string gPassword);
    Username();
    std::string getUsername();
    std::string getUsername( std::string gPassword);
    void setUsername(std::string username,std::string gUsername,std::string gPassword);
    bool isVisible();
    void setVisible(bool vis,std::string gPassword);

protected:
    GlobalID globalID;
    HashedCode username;
    std::string vUsername;
    bool visible;
};

#endif // USERNAME_H
