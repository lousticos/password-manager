#include "service.h"

Service::Service(std::string name,std::string type,std::string description):
    name(name),
    type(type),
    description(description)
{

}

Service::Service():
    name(""),
    type(""),
    description("")
{

}

std::string Service::getName(){
    return name;
}
std::string Service::getType(){
    return type;
}
std::string Service::getDesc(){
    return description;
}
void Service::setName(std::string name){
    this->name=name;
}
void Service::setType(std::string type){
    this->type=type;
}
void Service::setDesc(std::string description){
    this->description  = description;
}
