#include "mainwindow.h"
#include "elements.h"
#include "hidden.h"
#include <QApplication>
#include <iostream>
#include <QModelIndex>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();
    return a.exec();
}
