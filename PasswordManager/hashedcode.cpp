#include "hashedcode.h"
#include <math.h>
#include <iostream>


HashedCode::HashedCode(std::string code,std::string key)
{
    std::string resu;
    int nb = 0;

    int n = static_cast<int>(key.size());
    unsigned j = 0;
    for(char& c : code){
        nb = static_cast<int>(key[j])+static_cast<int>(c);
        if (nb > 127) nb = nb - 127;
        resu+=static_cast<char>(nb);
        j++;
        if (static_cast<int>(j)==n) j=0;
    }
    this->code = resu;
}
std::string HashedCode::getCode(){
    return this->code;
}

std::string HashedCode::getCode(std::string key)
{
    std::string resu;
    int nb = 0;
    int n = static_cast<int>(key.size());
    unsigned j = 0;
    for(char& c : code){
        nb = static_cast<int>(c)-static_cast<int>(key[j]);
        if (nb < 0) nb = nb + 127;
        resu+=static_cast<char>(nb);
        j++;

        if (static_cast<int>(j)==n) j=0;
    }

    return resu;
}

void HashedCode::setCode(std::string code, std::string key)
{

    std::string resu;
    int nb = 0;

    int n = static_cast<int>(key.size());
    unsigned j = 0;
    for(char& c : code){
        nb = static_cast<int>(key[j])+static_cast<int>(c);
        if (nb > 127) nb = nb - 127;
        resu+=static_cast<char>(nb);
        j++;
        if (static_cast<int>(j)==n) j=0;
    }
    this->code = resu;
}
