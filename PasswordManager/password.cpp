#include "password.h"

Password::Password(std::string password,std::string gUsername,std::string gPassword) :
    password(password,gPassword),
    globalID(gUsername,gPassword)
{
    securitylevel=1;
}

Password::Password():
    password("",""),
    globalID("","")
{
    securitylevel=1;
}

std::string Password::getCode(){
        return password.getCode();
}

std::string Password::getCode(std::string gUsername, std::string gPassword){
    if(globalID.verify(gUsername,gPassword)){
        return password.getCode(gPassword);
    }
    return "wrong";
}
int Password::getSecurityLevel(std::string gUsername,std::string gPassword){
    if(globalID.verify(gUsername,gPassword)){
        return securitylevel;
    }
    return 0;
}

void Password::setPassword(std::string password,std::string gUsername,std::string gPassword){
    if(this->globalID.verify(gUsername,gPassword)){
        this->password.setCode(password,gPassword);
    }
}
