#ifndef ELEMENTS_H
#define ELEMENTS_H

#include "element.h"
#include <vector>
#include <QFile>
#include <QAbstractTableModel>
#include <QJsonArray>
#include <QJsonValue>
#include <QJsonDocument>
#include <cstdlib>
#include <iostream>


class Elements : public QAbstractTableModel
{
    Q_OBJECT
public:
    Elements(QObject *parent = nullptr);
    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    int columnCount(const QModelIndex &parent = QModelIndex()) const;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const;
    Element* getElement(const QModelIndex &index);
    void addElement(Element element);
    void deleteElement(Element* element);
    int getSize();
    bool isEmpty();
    void clear();
    bool insertRows(int position, int rows, const QModelIndex &parent);
    bool removeRows(int position, int rows, const QModelIndex &parent);
    int importFile(QFile &filePath,QString gUsername,QString gPassword);
    void exportFile(QFile &filePath);
private:
    std::vector<Element> elementVector;
};

#endif // ELEMENTS_H
