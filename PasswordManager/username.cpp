#include "username.h"
#include <iostream>

Username::Username(std::string username, std::string gUsername, std::string gPassword) :
    globalID(gUsername,gPassword),
    username(username,gPassword),
    vUsername(username)
{
    visible=true;
}

Username::Username() :
    globalID("",""),
    username("",""),
    visible(true)
{

}

std::string Username::getUsername(){
    if(visible){
        return vUsername;
    }else{
        return username.getCode();
    }
}

std::string Username::getUsername(std::string gPassword){
    if(visible){
        return vUsername;
    }else{
        return username.getCode(gPassword);
    }
}

void Username::setUsername(std::string username,std::string gUsername, std::string gPassword)
{
    if(globalID.verify(gUsername,gPassword)){
            this->username.setCode(username,gPassword);
            this->vUsername = username;
    }
}

bool Username::isVisible(){
    return visible;
}

void Username::setVisible(bool vis,std::string gPassword){
        if(visible!=vis){
            if(vis){
                vUsername = username.getCode(gPassword);
            }else{
                vUsername = "";
            }
            visible=vis;
        }
}
