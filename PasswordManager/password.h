#ifndef PASSWORD_H
#define PASSWORD_H

#include "hashedcode.h"
#include "globalid.h"

class Password
{
public:
    Password(std::string password,std::string gUsername,std::string gPassword);
    Password();
    std::string getCode();
    std::string getCode(std::string gUsername, std::string gPassword);
    int getSecurityLevel(std::string gUsername,std::string gPassword);
    void setPassword(std::string password,std::string gUsername,std::string gPassword);
private:
    HashedCode password;
    int securitylevel;
    GlobalID globalID;
};

#endif // PASSWORD_H
