#ifndef ADD_H
#define ADD_H

#include <QDialog>

namespace Ui {
class Add;
}

class Add : public QDialog
{
    Q_OBJECT

public:
    explicit Add(QWidget *parent = nullptr);
    ~Add();
    QString getUsername();
    QString getPassword();
    QString getGUsername();
    QString getGPassword();
    QString getName();
    QString getType();
    QString getDescription();

private slots:
    void on_buttonBox_accepted();

private:
    Ui::Add *ui;
    QString username;
    QString password;
    QString gusername;
    QString gpassword;
    QString name;
    QString type;
    QString description;
};

#endif // ADD_H
